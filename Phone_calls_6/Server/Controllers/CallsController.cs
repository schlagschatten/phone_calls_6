﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phone_calls_6.Shared.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_calls_6.Server.Controllers
{
    [Route("Call")]
    [ApiController]
    public class CallsController : Controller
    {
        #region Private Fields

        private readonly PhoneCallsContext _context;

        #endregion Private Fields

        #region Public Constructors

        public CallsController(PhoneCallsContext context)
        {
            _context = context;
        }

        #endregion Public Constructors

        #region Public Methods

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Calls>>> GetCalls()
        {
            return await _context.Calls.ToListAsync();
        }

        // GET: Calls/Details/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Calls>> GetCalls(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Calls = await _context.Calls
                .FirstOrDefaultAsync(m => m.id == id);
            if (Calls == null)
            {
                return NotFound();
            }

            return Calls;
        }

        [HttpPut]
        public async Task<IActionResult> PutCall(Calls call)
        {
            _context.Entry(call).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CallsExists(call.id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Calls>> PostCall(Calls call)
        {
            _context.Calls.Add(call);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCalls", new { call.id }, call);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Calls>> DeleteCall(int id)
        {
            var Calls = await _context.Calls.FindAsync(id);
            _context.Calls.Remove(Calls);
            await _context.SaveChangesAsync();
            return Calls;
        }

        #endregion Public Methods

        #region Private Methods

        private bool CallsExists(int id)
        {
            return _context.Calls.Any(e => e.id == id);
        }

        #endregion Private Methods
    }
}