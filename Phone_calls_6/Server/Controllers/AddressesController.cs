﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phone_calls_6.Shared.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_calls_6.Server.Controllers
{
    [Route("Address")]
    [ApiController]
    public class AddressesController : Controller
    {
        #region Private Fields

        private readonly PhoneCallsContext _context;

        #endregion Private Fields

        #region Public Constructors

        public AddressesController(PhoneCallsContext context)
        {
            _context = context;
        }

        #endregion Public Constructors

        #region Public Methods

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Address>>> GetAddress()
        {
            return await _context.Addresses.ToListAsync();
        }

        // GET: Address/Details/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Address>> GetAddress(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Address = await _context.Addresses
                .FirstOrDefaultAsync(m => m.id == id);
            if (Address == null)
            {
                return NotFound();
            }

            return Address;
        }

        [HttpPut]
        public async Task<IActionResult> PutAddress(Address address)
        {
            _context.Entry(address).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AddressExists(address.id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Address>> PostAddress(Address address)
        {
            _context.Addresses.Add(address);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAddress", new { address.id }, address);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Address>> DeleteAddress(int id)
        {
            var Address = await _context.Addresses.FindAsync(id);
            _context.Addresses.Remove(Address);
            await _context.SaveChangesAsync();
            return Address;
        }

        #endregion Public Methods

        #region Private Methods

        private bool AddressExists(int id)
        {
            return _context.Addresses.Any(e => e.id == id);
        }

        #endregion Private Methods
    }
}