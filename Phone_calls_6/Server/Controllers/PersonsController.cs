﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phone_calls_6.Shared.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_calls_6.Server.Controllers
{
    [Route("Person")]
    [ApiController]
    public class PersonsController : Controller
    {
        #region Private Fields

        private readonly PhoneCallsContext _context;

        #endregion Private Fields

        #region Public Constructors

        public PersonsController(PhoneCallsContext context)
        {
            _context = context;
        }

        #endregion Public Constructors

        #region Public Methods

        // GET: Persons
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Persons>>> GetPersons()
        {
            return await _context.Persons.ToListAsync();
        }

        // GET: Persons/Details/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Persons>> GetPersons(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Persons = await _context.Persons
                .FirstOrDefaultAsync(m => m.id == id);
            if (Persons == null)
            {
                return NotFound();
            }

            return Persons;
        }

        [HttpPut]
        public async Task<IActionResult> PutPerson(Persons person)
        {
            _context.Entry(person).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonsExists(person.id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Persons>> PostPerson(Persons persons)
        {
            _context.Persons.Add(persons);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPersons", new { persons.id }, persons);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Persons>> DeletePerson(int id)
        {
            var Persons = await _context.Persons.FindAsync(id);
            _context.Persons.Remove(Persons);
             _context.SaveChanges();
            return Persons;
        }

        #endregion Public Methods

        #region Private Methods

        private bool PersonsExists(int id)
        {
            return _context.Persons.Any(e => e.id == id);
        }

        #endregion Private Methods
    }
}