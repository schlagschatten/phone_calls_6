using Microsoft.EntityFrameworkCore;

namespace Phone_calls_6.Shared.Models
{
    public class PhoneCallsContext : DbContext
    {
        #region Public Constructors

        public PhoneCallsContext(DbContextOptions<PhoneCallsContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        #endregion Public Constructors

        #region Public Properties

        public DbSet<Calls> Calls { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Persons> Persons { get; set; }

        #endregion Public Properties
    }
}