using System.Collections.Generic;

namespace Phone_calls_6.Shared.Models
{
    public class Address
    {
        #region Public Properties

        public int id { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string house_number { get; set; }
        public List<Persons> persons { get; set; }

        #endregion Public Properties
    }
}