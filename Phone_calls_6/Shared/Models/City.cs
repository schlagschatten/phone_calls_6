using System.Collections.Generic;

namespace Phone_calls_6.Shared.Models
{
    public class City
    {
        #region Public Properties

        public int id { get; set; }
        public decimal price { get; set; }
        public string city { get; set; }
        public int code { get; set; }
        public List<Calls> calls { get; set; }

        #endregion Public Properties
    }
}